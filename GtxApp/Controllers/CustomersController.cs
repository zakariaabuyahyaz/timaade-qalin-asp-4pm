﻿using GtxApp.Data;
using GtxApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GtxApp.Controllers
{
    [Authorize]
    public class CustomersController : Controller
    {
        private readonly qalinContext _context;

        public CustomersController()
        {
            _context= new qalinContext();
        }
        
        public IActionResult Index()        //action
        {
            // read customers list from the db
            var _list = _context.Customers.ToList();
            return View(_list);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();  
        }

        [HttpPost]
        public IActionResult Create([Bind("Fullname, Mobile, Email, Address")]Customer model)
        {
            //save to database
            _context.Customers.Add(model);
            _context.SaveChanges(); 
            return RedirectToAction("Index");   
        }

        //[Authorize(Roles ="superadmin")]
        public IActionResult Edit(int id)
        {
            var _found = _context.Customers.Find(id);
            return View(_found);

        }

        //[Authorize(Roles = "superadmin")]
        [HttpPost]  //action verbs   -- action selectors
        public IActionResult Edit([Bind("Id, Fullname, Mobile, Email, Address")] Customer model)
        {
            //save to database
            Customer _found = _context.Customers.Find(model.Id);
            _found.Fullname = model.Fullname;
            _found.Address = model.Address;
            _found.Email= model.Email;  
            _found.Mobile= model.Mobile;
            _context.Customers.Update(_found);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id) { 
            var found = _context.Customers.Find(id);
            return View(found);
        }

        [HttpPost]
        public IActionResult Delete([Bind("Id")] Customer model)
        {
            var found = _context.Customers.Find(model.Id);
            _context.Customers.Remove(found);
            _context.SaveChanges();
            return RedirectToAction("Index");   
        }

    }
}
