﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GtxApp.Data;
using GtxApp.Models;
using Microsoft.AspNetCore.Authorization;

namespace GtxApp.Controllers
{
    [Authorize]
    public class BorrowedbooksController : Controller
    {
        private readonly qalinContext _context;

        public BorrowedbooksController(qalinContext context)
        {
            _context = context;
        }

        // GET: Borrowedbooks
        public IActionResult Index()
        {
            var _data = _context.Borrowedbooks
                .Include(a=>a.Book)
                .Include(b=>b.Customer)
                .OrderByDescending(p=>p.Id)
                .ToList();
            return View(_data);
        }

        // GET: Borrowedbooks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Borrowedbooks == null)
            {
                return NotFound();
            }

            var borrowedbook = await _context.Borrowedbooks
                .Include(b => b.Book)
                .Include(b => b.Customer)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (borrowedbook == null)
            {
                return NotFound();
            }

            return View(borrowedbook);
        }

        // GET: Borrowedbooks/Create
        public IActionResult Create()
        {
            ViewData["Bookid"] = new SelectList(_context.Books, "Id", "Id");
            ViewData["Customerid"] = new SelectList(_context.Customers, "Id", "Id");
            return View();
        }

        // POST: Borrowedbooks/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Bookid,Customerid,Borroweddate,Returndate,Status,Ruser,Registereddate")] Borrowedbook borrowedbook)
        {
            if (ModelState.IsValid)
            {
                _context.Add(borrowedbook);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Bookid"] = new SelectList(_context.Books, "Id", "Id", borrowedbook.Bookid);
            ViewData["Customerid"] = new SelectList(_context.Customers, "Id", "Id", borrowedbook.Customerid);
            return View(borrowedbook);
        }

        // GET: Borrowedbooks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Borrowedbooks == null)
            {
                return NotFound();
            }

            var borrowedbook = await _context.Borrowedbooks.FindAsync(id);
            if (borrowedbook == null)
            {
                return NotFound();
            }
            ViewData["Bookid"] = new SelectList(_context.Books, "Id", "Id", borrowedbook.Bookid);
            ViewData["Customerid"] = new SelectList(_context.Customers, "Id", "Id", borrowedbook.Customerid);
            return View(borrowedbook);
        }

        // POST: Borrowedbooks/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Bookid,Customerid,Borroweddate,Returndate,Status,Ruser,Registereddate")] Borrowedbook borrowedbook)
        {
            if (id != borrowedbook.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(borrowedbook);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BorrowedbookExists(borrowedbook.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Bookid"] = new SelectList(_context.Books, "Id", "Id", borrowedbook.Bookid);
            ViewData["Customerid"] = new SelectList(_context.Customers, "Id", "Id", borrowedbook.Customerid);
            return View(borrowedbook);
        }

        // GET: Borrowedbooks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Borrowedbooks == null)
            {
                return NotFound();
            }

            var borrowedbook = await _context.Borrowedbooks
                .Include(b => b.Book)
                .Include(b => b.Customer)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (borrowedbook == null)
            {
                return NotFound();
            }

            return View(borrowedbook);
        }

        // POST: Borrowedbooks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Borrowedbooks == null)
            {
                return Problem("Entity set 'qalinContext.Borrowedbooks'  is null.");
            }
            var borrowedbook = await _context.Borrowedbooks.FindAsync(id);
            if (borrowedbook != null)
            {
                _context.Borrowedbooks.Remove(borrowedbook);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BorrowedbookExists(int id)
        {
          return (_context.Borrowedbooks?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
