﻿using GtxApp.Data;
using GtxApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GtxApp.Controllers
{
    [Authorize]
    public class BooksController : Controller
    {
        private readonly qalinContext _db;
        public BooksController()
        {
            _db = new qalinContext();
        }
        public IActionResult Index()
        {
            var _bookslist = _db.Books.OrderByDescending(m => m.Id).ToList();
            return View(_bookslist);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost] //action verb
        public IActionResult Create([Bind("Title, Author, Publisher, Availablecopies")] Book model)
        {
            model.Registereddate = DateTime.Now;
            model.Ruser = "Zakaria";
            _db.Books.Add(model);
            _db.SaveChanges();
            return RedirectToAction("Index");

        }

        public IActionResult Edit(int id)
        {
            var found = _db.Books.Find(id);
            return View(found); 
        }

        [HttpPost] //action verb
        public IActionResult Edit([Bind("Id, Title, Author, Publisher, Availablecopies")] Book model)
        {
            var _found = _db.Books.Find(model.Id);
            _found.Title=model.Title;
            _found.Author=model.Author;
            _found.Publisher=model.Publisher;   
            _found.Availablecopies=model.Availablecopies;
            _db.Books.Update(_found);
            _db.SaveChanges();
            return RedirectToAction("Index");

        }

        public IActionResult Delete(int id)
        {
            var _found = _db.Books.Find(id);
            return View(_found);
        }

        [HttpPost]
        public IActionResult Delete([Bind("Id, Title, Author")]Book model)
        {
            var _found = _db.Books.Find(model.Id);
            _db.Books.Remove(_found);
            _db.SaveChanges();
            return RedirectToAction("Index");

        }

    }
}
