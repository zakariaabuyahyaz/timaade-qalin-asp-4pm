﻿using System;
using System.Collections.Generic;
using GtxApp.Models;
using Microsoft.EntityFrameworkCore;

namespace GtxApp.Data;

public partial class qalinContext : DbContext
{
    public qalinContext()
    {
    }

    public qalinContext(DbContextOptions<qalinContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Book> Books { get; set; }

    public virtual DbSet<Borrowedbook> Borrowedbooks { get; set; }

    public virtual DbSet<Customer> Customers { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("server=localhost; initial catalog=qalin_db; user id=sa; password=per457@.Q;TrustServerCertificate=True");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Book>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__books__3213E83F44B925A9");

            entity.ToTable("books");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Author)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("author");
            entity.Property(e => e.Availablecopies).HasColumnName("availablecopies");
            entity.Property(e => e.Isbn)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("isbn");
            entity.Property(e => e.Publisheddate)
                .HasColumnType("date")
                .HasColumnName("publisheddate");
            entity.Property(e => e.Publisher)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("publisher");
            entity.Property(e => e.Registereddate)
                .HasColumnType("smalldatetime")
                .HasColumnName("registereddate");
            entity.Property(e => e.Ruser)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("ruser");
            entity.Property(e => e.Title)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("title");
        });

        modelBuilder.Entity<Borrowedbook>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__borrowed__3213E83F4BADA21E");

            entity.ToTable("borrowedbooks");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Bookid).HasColumnName("bookid");
            entity.Property(e => e.Borroweddate)
                .HasColumnType("date")
                .HasColumnName("borroweddate");
            entity.Property(e => e.Customerid).HasColumnName("customerid");
            entity.Property(e => e.Registereddate)
                .HasColumnType("smalldatetime")
                .HasColumnName("registereddate");
            entity.Property(e => e.Returndate)
                .HasColumnType("date")
                .HasColumnName("returndate");
            entity.Property(e => e.Ruser)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("ruser");
            entity.Property(e => e.Status)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("status");

            entity.HasOne(d => d.Book).WithMany(p => p.Borrowedbooks)
                .HasForeignKey(d => d.Bookid)
                .HasConstraintName("FK__borrowedb__booki__145C0A3F");

            entity.HasOne(d => d.Customer).WithMany(p => p.Borrowedbooks)
                .HasForeignKey(d => d.Customerid)
                .HasConstraintName("FK__borrowedb__custo__15502E78");
        });

        modelBuilder.Entity<Customer>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__customer__3213E83F069FE9E6");

            entity.ToTable("customers");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Address)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("address");
            entity.Property(e => e.Email)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("email");
            entity.Property(e => e.Fullname)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("fullname");
            entity.Property(e => e.Mobile)
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasColumnName("mobile");
            entity.Property(e => e.Registereddate)
                .HasColumnType("smalldatetime")
                .HasColumnName("registereddate");
            entity.Property(e => e.Ruser)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("ruser");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
