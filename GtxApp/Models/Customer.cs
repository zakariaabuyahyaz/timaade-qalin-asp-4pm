﻿using System;
using System.Collections.Generic;

namespace GtxApp.Models;

public partial class Customer
{
    public int Id { get; set; }

    public string? Fullname { get; set; }

    public string? Address { get; set; }

    public string? Email { get; set; }

    public string? Mobile { get; set; }

    public string? Ruser { get; set; }

    public DateTime? Registereddate { get; set; }

    public virtual ICollection<Borrowedbook> Borrowedbooks { get; set; } = new List<Borrowedbook>();
}
