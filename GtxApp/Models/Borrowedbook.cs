﻿using System;
using System.Collections.Generic;

namespace GtxApp.Models;

public partial class Borrowedbook
{
    public int Id { get; set; }

    public int? Bookid { get; set; }

    public int? Customerid { get; set; }

    public DateTime? Borroweddate { get; set; }

    public DateTime? Returndate { get; set; }

    public string? Status { get; set; }

    public string? Ruser { get; set; }

    public DateTime? Registereddate { get; set; }

    public virtual Book? Book { get; set; }

    public virtual Customer? Customer { get; set; }
}
